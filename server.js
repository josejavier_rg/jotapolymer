const express = require('express');
const path = require('path');
const PORT = 8081;
const app = express();

app.use(express.static(__dirname));

app.listen(PORT);
console.log("Express funcionando en puerto" + PORT);
